package names

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestJoin(t *testing.T) {
	res := Join(" ", "foo", "bar")
	assert.Equal(t, "foo bar", res)

	lab := Label("ding", "dong")
	assert.Equal(t, "ding-dong", lab)
}
