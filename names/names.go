package names

import (
	"strings"
)

func Label(s ...string) string {
	return Join("-", s...)
}

func Join(sep string, parts ...string) string {
	return strings.Join(parts, sep)
}

// FreeBSD 11.3-RELEASE-amd64-bc4e6908-719b-4c0d-bab6-d5e6a707dbe3-ami-0b96e8856151afb3a.4
// available from Marketplace in eu-west-1
const (
	Freebsd11 = "ami-0c52a982d87b6deb1"
	AwsLinux  = "ami-0ce71448843cb18a1"
)
