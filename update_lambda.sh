name="${1}"

GOOS=linux go build -o /tmp/"${name}" bitbucket.org/geomacy/awsgo/news/cmd/"${name}"
zip -j /tmp/"${name}".zip /tmp/"${name}"
aws lambda update-function-code \
--function-name "${name}" \
--zip-file fileb:///tmp/"${name}".zip