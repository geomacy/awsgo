# awsgo

A "Good News" detection serverless application in Go.

Store sources for news in DynamoDB. Poll them periodically for new news articles. If the articles contain good news, send an email to the user with a summary!

See diagram https://app.cloudcraft.co/blueprint/06c2c1f2-4d24-452c-b49e-e7f38c585c28

![architecture diagram](News.png)

# Notes and Thoughts

Refer to https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/welcome.html

Use Goland

Make sure to inject the API interface when creating all types - aids testability; thought it's nice to be able 
to test with live AWS very simply using the `session.SharedConfigEnable` motif.


Some of the APIs are not too informative if there is an error. E.g. try a delete item in DynamoDB
and get the query wrong, you get back an error
```
Expected nil, but got: &awserr.requestError{awsError:(*awserr.baseError)(0xc0000b6480), statusCode:400, requestID:"EH454FBCCV5QQH2CC9241S0VAJVV4KQNSO5AEMVJF66Q9ASUAAJG", bytes:[]uint8(nil)}
```
which isn't helpful

Handle errors from AWS operations as 
```go
if aerr, ok := err.(awserr.Error); ok {
	aerr.Code()
    aerr.Message()
}
```
