#!/bin/bash

aws dynamodb create-table --table-name Feeds \
--attribute-definitions \
  AttributeName=Name,AttributeType=S \
--key-schema \
  AttributeName=Name,KeyType=HASH \
--provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=10