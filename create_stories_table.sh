#!/bin/bash

aws dynamodb create-table --table-name Stories \
  --attribute-definitions \
  AttributeName=URL,AttributeType=S \
  --key-schema \
  AttributeName=URL,KeyType=HASH \
  --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=10


