package news

import (
	"fmt"
	"time"
)

const (
	UrlKey           = "URL"
	TitleKey         = "title"
	PublishedKey     = "published"
	UpdatedKey       = "updated"
	CategoryKey      = "category"
	SerialTimeFormat = time.RFC3339
)

type Category int

const (
	Unknown Category = iota
	Good
	Bad
	Ugly
)

func (c Category) Value() int {
	return int(c)
}

func CategoryOf(s string) Category {
	switch s {
	case "good":
		return Good
	case "bad":
		return Bad
	case "ugly":
		return Ugly
	default:
		return Unknown
	}
}

func (c Category) String() string {
	switch c {
	case Good:
		return "good"
	case Bad:
		return "bad"
	case Ugly:
		return "ugly"
	default:
		return "unknown"
	}
}

type Story struct {
	Url, Title         string
	Published, Updated time.Time
	Category           Category
}

// CreateStory creates a story whose category is known
func CreateStory(href, title, publishedTime, updatedTime string, category Category) (story Story, err error) {
	var published, updated time.Time
	if published, err = Unmarshal(publishedTime); err != nil {
		return Story{}, fmt.Errorf("unrecognised time format: %w", err)
	}
	if updated, err = Unmarshal(updatedTime); err != nil {
		return Story{}, fmt.Errorf("unrecognised time format: %w", err)
	}
	story = Story{
		href,
		title,
		published,
		updated,
		category,
	}
	return
}

// NewStory creates a record for a story whose category is not known
func NewStory(href, title, publishedTime, updatedTime string) (story Story, err error) {
	return CreateStory(href, title, publishedTime, updatedTime, Unknown)
}

func Marshal(t time.Time) string {
	return t.Format(SerialTimeFormat)
}

func Unmarshal(s string) (time.Time, error) {
	return time.Parse(SerialTimeFormat, s)
}

func (s Story) PublishedTime() string {
	return Marshal(s.Published)
}

func (s Story) UpdatedTime() string {
	return Marshal(s.Updated)
}
