package collector

import (
	"bitbucket.org/geomacy/awsgo/news"
	"fmt"
	"io/ioutil"
	"strings"
)

func analyseStory(story news.Story) (news.Category, error) {
	resource, err := news.GetResource(story.Url)
	if err != nil {
		return news.Unknown, fmt.Errorf("failed to get story %s (%s): %w", story.Title, story.Url, err)
	}
	defer func() { _ = resource.Body.Close() }()
	bytes, err := ioutil.ReadAll(resource.Body)
	if err != nil {
		return news.Unknown, fmt.Errorf("failed to read story %s (%s): %w", story.Title, story.Url, err)
	}
	text := string(bytes)

	decision := analyseSentiment(text)
	fmt.Printf("Story '%s' categorised as %s\n", story.Title, decision.String())
	return decision, nil
}

func analyseSentiment(text string) news.Category {
	if strings.Contains(text, "happy") ||
		strings.Contains(text, "rescued") ||
		strings.Contains(text, "cured") ||
		strings.Contains(text, "kittens") {

		return news.Good
	} else {
		return news.Bad
	}
}
