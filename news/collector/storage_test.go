package collector

import (
	"bitbucket.org/geomacy/awsgo"
	"bitbucket.org/geomacy/awsgo/news"
	"fmt"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/matryer/is"
	"sort"
	"testing"
	"time"
)

func TestStoreStory(t *testing.T) {

	is := is.New(t)

	sess := awsgo.CreateSession()
	ddb := dynamodb.New(sess)
	client := NewStoryDatabase(ddb)

	gmt, _ := time.LoadLocation("Europe/London")
	published := time.Date(2020, 1, 2, 0, 0, 0, 0, gmt)
	var updated time.Time
	_, err := client.AddStory(news.Story{
		Url:       "https://www.bbc.co.uk/news/business-51113895",
		Title:     "Flybe: Government strikes a deal",
		Published: published,
		Updated:   updated,
		Category:  news.Good,
	})
	is.NoErr(err)

	published2 := time.Date(2020, 1, 3, 0, 0, 0, 0, gmt)
	updated = time.Date(2020, 1, 4, 0, 0, 0, 0, gmt)
	_, err = client.AddStory(news.Story{
		Url:       "https://www.bbc.co.uk/news/entertainment-arts-51112742",
		Title:     "Billie Eilish to sing the new James Bond theme",
		Published: published2,
		Updated:   updated,
		Category:  news.Good,
	})
	is.NoErr(err)

	stories, err := client.GetStories()
	is.NoErr(err)
	is.Equal(2, len(stories))

	sort.Slice(stories, func(i, j int) bool {
		return stories[i].Title < stories[j].Title
	})

	is.Equal("Billie Eilish to sing the new James Bond theme", stories[0].Title)
	is.Equal("https://www.bbc.co.uk/news/entertainment-arts-51112742", stories[0].Url)
	is.Equal("Flybe: Government strikes a deal", stories[1].Title)
	is.Equal("https://www.bbc.co.uk/news/business-51113895", stories[1].Url)

	is.Equal(published2.Format(news.SerialTimeFormat), stories[0].Published.Format(news.SerialTimeFormat))
	is.Equal(updated.Format(news.SerialTimeFormat), stories[0].Updated.Format(news.SerialTimeFormat))

	is.Equal(published.Format(news.SerialTimeFormat), stories[1].Published.Format(news.SerialTimeFormat))
	is.True(stories[1].Updated.IsZero())

	for _, s := range stories {
		_, err := client.DeleteStory(s)
		if err != nil {
			fmt.Printf("error deleting %s: %s\n", s.Url, err)
			continue
		}
	}

	stories, err = client.GetStories()
	is.NoErr(err)
	is.Equal(0, len(stories))

}

func TestGetStories(t *testing.T) {

	is := is.New(t)

	sess := awsgo.CreateSession()
	ddb := dynamodb.New(sess)
	client := NewStoryDatabase(ddb)

	stories, err := client.GetStories()
	is.NoErr(err)
	for _, s := range stories {
		fmt.Printf("%s\n%s\nPublished: %s, Updated: %s\nCategory: %s\n\n",
			s.Title,
			s.Url,
			s.PublishedTime(),
			s.UpdatedTime(),
			s.Category.String(),
		)
	}
}
