package collector

import (
	"bitbucket.org/geomacy/awsgo/news"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"sync"
)

type Collector struct {
	sqsiface.SQSAPI
	queueURL *string
	db       StoryDatabase
}

func CreateCollector(sqsAPI sqsiface.SQSAPI, ddbAPI dynamodbiface.DynamoDBAPI, queue string) (Collector, error) {
	queueRef, err := sqsAPI.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName: aws.String(queue),
	})
	if err != nil {
		return Collector{}, fmt.Errorf("could not get source %s: %w", queue, err)
	}
	database := NewStoryDatabase(ddbAPI)
	return Collector{
		SQSAPI:   sqsAPI,
		queueURL: queueRef.QueueUrl,
		db:       database,
	}, nil
}

func (c Collector) CollectStories() error {

	count := 0
	for {
		size, err := c.collectBatch()
		if err != nil {
			return fmt.Errorf("execution failed: %w", err)
		}
		count += size

		if size == 0 {
			fmt.Printf("%d stories processed\n", count)
			return nil
		}
	}
}

func (c Collector) collectBatch() (processed int, err error) {
	processed = 0
	result, err := c.readBatchFromSource()
	if err != nil {
		return 0, fmt.Errorf("failed to load from source queue: %w", err)
	}

	batchSize := len(result.Messages)
	if batchSize == 0 {
		return
	}

	fmt.Printf("processing batch of %d messages\n", batchSize)
	var wg sync.WaitGroup
	errc := make(chan error, batchSize)
	wg.Add(batchSize)
	for _, message := range result.Messages {
		go func(m *sqs.Message) {
			errc <- c.collectStory(m)
			wg.Done()
		}(message)
	}
	wg.Wait()

	var errorCount int
	for c := 0; c < batchSize; c++ {
		select {
		case e := <-errc:
			if e != nil {
				fmt.Printf("failed to collect story: %s\n", e)
				errorCount += 1
			}
		default:
		}
	}
	processed = batchSize
	return
}

func (c Collector) collectStory(m *sqs.Message) (err error) {

	fmt.Printf("processing message: %s\n", *m.ReceiptHandle)

	defer func() {
		_, e := c.DeleteMessage(&sqs.DeleteMessageInput{
			QueueUrl:      c.queueURL,
			ReceiptHandle: m.ReceiptHandle,
		})

		if e != nil {
			if err != nil {
				err = fmt.Errorf("failed to delete message because '%s' following: %w", e, err)
			} else {
				err = e
			}
		}
	}()

	story, err := unmarshalStory(m)
	if err != nil {
		return fmt.Errorf("failed to parse message %s: %w", *m.ReceiptHandle, err)
	}

	fmt.Printf("processing story: '%s' (%s)\n", story.Title, story.Url)

	recorded, exists, err := c.db.GetStory(story.Url)
	if err != nil {
		return fmt.Errorf("database failure retrieving story %s: %w", story.Url, err)
	}

	if exists {
		if recorded.Category == news.Bad {
			return nil
		}

		if story.Updated.After(recorded.Updated) {
			c.db.Update(recorded.Url, story)
		}

	} else {
		category, err := analyseStory(story)
		if err != nil {
			return fmt.Errorf("analysis failed for story %s: %w", story.Url, err)
		}
		story.Category = category

		_, err = c.db.AddStory(story)
		if err != nil {
			return fmt.Errorf("could not store new story %s: %w", story.Url, err)
		}
	}

	return nil
}

func unmarshalStory(m *sqs.Message) (news.Story, error) {
	getStringAttribute := func(key string) (string, error) {
		if s, ok := m.MessageAttributes[key]; ok {
			return *s.StringValue, nil
		}
		return "", fmt.Errorf("could not find message attribute %s", key)
	}

	href, err := getStringAttribute(news.UrlKey)
	if err != nil {
		return news.Story{}, err
	}

	published, err := getStringAttribute(news.PublishedKey)
	if err != nil {
		return news.Story{}, err
	}

	updated, err := getStringAttribute(news.UpdatedKey)
	if err != nil {
		return news.Story{}, err
	}

	title := *m.Body
	if err != nil {
		return news.Story{}, err
	}

	story, err := news.NewStory(href, title, published, updated)
	if err != nil {
		return news.Story{}, fmt.Errorf("could not construct story from message attributes")
	}

	return story, nil
}

func (c Collector) readBatchFromSource() (*sqs.ReceiveMessageOutput, error) {
	return c.ReceiveMessage(&sqs.ReceiveMessageInput{
		AttributeNames: []*string{
			aws.String(sqs.MessageSystemAttributeNameSentTimestamp),
		},
		MessageAttributeNames: []*string{
			aws.String(sqs.QueueAttributeNameAll),
		},
		QueueUrl:            c.queueURL,
		MaxNumberOfMessages: aws.Int64(10),
		VisibilityTimeout:   aws.Int64(20), // 20 seconds
		WaitTimeSeconds:     aws.Int64(0),
	})
}
