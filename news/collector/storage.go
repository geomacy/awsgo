package collector

import (
	"fmt"
	"time"

	"bitbucket.org/geomacy/awsgo/news"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"

	ex "github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

const (
	storyTable = "Stories"
)

type StoryDatabase struct {
	dynamodbiface.DynamoDBAPI
}

func NewStoryDatabase(ddbAPI dynamodbiface.DynamoDBAPI) StoryDatabase {
	return StoryDatabase{ddbAPI}
}

func (ddb StoryDatabase) AddStory(story news.Story) (*dynamodb.PutItemOutput, error) {

	published := story.PublishedTime()
	updated := story.UpdatedTime()
	category := story.Category.String()
	storyAtts := map[string]*dynamodb.AttributeValue{
		news.UrlKey:       {S: &story.Url},
		news.TitleKey:     {S: &story.Title},
		news.PublishedKey: {S: &published},
		news.UpdatedKey:   {S: &updated},
		news.CategoryKey:  {S: &category},
	}

	storyItem := dynamodb.PutItemInput{
		TableName: aws.String(storyTable),
		Item:      storyAtts,
	}
	output, err := ddb.PutItem(&storyItem)
	return output, err
}

func (ddb StoryDatabase) GetStory(href string) (story news.Story, found bool, err error) {
	return news.Story{}, false, nil
}

func (ddb StoryDatabase) Update(href string, story news.Story) error {
	return nil
}

const Day = 24 * time.Hour

func (ddb StoryDatabase) GetStories() ([]news.Story, error) {

	dayAgo := time.Now().Add(time.Duration(-24) * time.Hour)
	yesterday := news.Marshal(dayAgo)

	// https://aws.amazon.com/blogs/developer/introducing-amazon-dynamodb-expression-builder-in-the-aws-sdk-for-go/
	recentlyChanged := ex.Or(
		ex.Name(news.UpdatedKey).GreaterThan(ex.Value(yesterday)),
		ex.Name(news.PublishedKey).GreaterThan(ex.Value(yesterday)),
	)
	goodNews := ex.Name(news.CategoryKey).Equal(ex.Value(news.Good.String()))

	expr, err := ex.NewBuilder().WithFilter(
		ex.And(goodNews, recentlyChanged),
	).Build()
	if err != nil {
		fmt.Println(err)
	}

	result, err := ddb.Scan(&dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		TableName:                 aws.String(storyTable),
	})
	if err != nil {
		return nil, fmt.Errorf("failed to read table %s: %s", storyTable, err)
	}

	results := make([]news.Story, len(result.Items))
	for i, it := range result.Items {

		category := news.CategoryOf(*it[news.CategoryKey].S)
		story, err := news.CreateStory(
			*it[news.UrlKey].S,
			*it[news.TitleKey].S,
			*it[news.PublishedKey].S,
			*it[news.UpdatedKey].S,
			category,
		)
		if err != nil {
			fmt.Printf("failed to build story: %s\n", err)
			continue
		}
		results[i] = story
	}
	return results, nil
}

func (ddb StoryDatabase) DeleteStory(story news.Story) (*dynamodb.DeleteItemOutput, error) {

	storyAtts := map[string]*dynamodb.AttributeValue{
		news.UrlKey: {S: &story.Url},
	}

	feedItem := dynamodb.DeleteItemInput{
		TableName: aws.String(storyTable),
		Key:       storyAtts,
	}
	output, err := ddb.DeleteItem(&feedItem)
	return output, err
}
