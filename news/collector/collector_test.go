package collector

import (
	"fmt"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sqs"
	"testing"

	"bitbucket.org/geomacy/awsgo"
	"bitbucket.org/geomacy/awsgo/news/watcher"
	"github.com/matryer/is"
)

func TestCollectBatch(t *testing.T) {
	is := is.New(t)
	sess := awsgo.CreateSession()
	sqsAPI := sqs.New(sess)
	ddb := dynamodb.New(sess)
	c, err := CreateCollector(sqsAPI, ddb, watcher.QueueName)
	is.NoErr(err)

	n, err := c.collectBatch()
	is.NoErr(err)
	fmt.Printf("%d stories processed in batch\n", n)
}

func TestCollectStories(t *testing.T) {
	is := is.New(t)
	sess := awsgo.CreateSession()
	sqsAPI := sqs.New(sess)
	ddb := dynamodb.New(sess)

	c, err := CreateCollector(sqsAPI, ddb, watcher.QueueName)
	is.NoErr(err)

	err = c.CollectStories()
	is.NoErr(err)
}
