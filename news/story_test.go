package news

import (
	"testing"

	"github.com/matryer/is"
)

func TestCategoryConversion(t *testing.T) {
	is := is.New(t)

	recycling := CategoryOf("good")
	is.Equal(recycling, Good)

	pollution := Bad
	is.Equal(pollution.String(), "bad")

	whereabouts := CategoryOf("Bali")
	is.Equal(whereabouts, Unknown)
}
