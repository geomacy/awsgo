package reporter

import (
	"bitbucket.org/geomacy/awsgo/news"
	"bitbucket.org/geomacy/awsgo/news/collector"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/sesv2"
	"github.com/aws/aws-sdk-go/service/sesv2/sesv2iface"
	"html/template"
	"os"
	"strings"
)

type Reporter struct {
	sesv2iface.SESV2API
	db collector.StoryDatabase
}

func NewReporter(sesAPI sesv2iface.SESV2API, ddbAPI dynamodbiface.DynamoDBAPI) Reporter {
	return Reporter{
		SESV2API: sesAPI,
		db:       collector.NewStoryDatabase(ddbAPI),
	}
}

func (r Reporter) Run() error {

	var sender, recipient string
	var found bool
	if recipient, found = os.LookupEnv("RECIPIENT"); !found {
		return fmt.Errorf("no recipient email specified in RECIPIENT env var")
	}
	if sender, found = os.LookupEnv("SENDER"); !found {
		return fmt.Errorf("no recipient email specified in RECIPIENT env var")
	}

	stories, err := r.db.GetStories()
	if err != nil {
		return fmt.Errorf("failed to retrieve stories: %w", err)
	}
	if len(stories) == 0 {
		return nil
	}

	text, err := toHTML(stories)
	if err != nil {
		return fmt.Errorf("failed to render email text: %w", err)
	}

	err = r.sendMail(sender, recipient, text)
	if err != nil {
		return fmt.Errorf("failed to send mail to %s: %w", recipient, err)
	}

	return nil
}

const storyMarkup = `
<div><a href="{{.Url}}">{{.Title}}</a></div>
`

func toHTML(stories []news.Story) (string, error) {

	t, err := template.New("foo").Parse(storyMarkup)
	if err != nil {
		return "", fmt.Errorf("failed to parse HTML template: %w", err)
	}

	var builder strings.Builder
	for _, story := range stories {
		err = t.Execute(&builder, story)
		if err != nil {
			fmt.Printf("failed to mark up story '%s' (skipping): %s", story.Title, err)
			continue
		}
	}

	return builder.String(), nil
}

func (r Reporter) sendMail(sender, recipient, text string) error {

	utf8 := aws.String("UTF-8")
	input := &sesv2.SendEmailInput{
		FromEmailAddress: aws.String(sender),
		Destination: &sesv2.Destination{
			CcAddresses: []*string{},
			ToAddresses: []*string{
				aws.String(recipient),
			},
		},
		Content: &sesv2.EmailContent{
			Simple: &sesv2.Message{
				Body: &sesv2.Body{
					Html: &sesv2.Content{
						Charset: utf8,
						Data:    aws.String(text),
					},
					Text: nil,
				},
				Subject: &sesv2.Content{
					Charset: utf8,
					Data:    aws.String("here is some good news"),
				},
			},
		},
	}

	// Attempt to send the email.
	_, err := r.SendEmail(input)
	if err != nil {
		return fmt.Errorf("failed to send email to %s: %w", recipient, err)
	}

	return nil
}
