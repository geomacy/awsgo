package reporter

import (
	"bitbucket.org/geomacy/awsgo/news"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"bitbucket.org/geomacy/awsgo"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sesv2"
	"github.com/matryer/is"
)

func TestSendMail(t *testing.T) {
	is := is.New(t)
	sess := awsgo.CreateSession()
	ddb := dynamodb.New(sess)
	ses := sesv2.New(sess)

	reporter := NewReporter(ses, ddb)
	sender, found := os.LookupEnv("SENDER")
	is.True(found)
	recipient, found := os.LookupEnv("RECIPIENT")
	is.True(found) // Please supply recipient email in RECIPIENT env var

	err := reporter.sendMail(sender, recipient, "hello <b>test<b>")
	is.NoErr(err)
}

func TestToHTML(tst *testing.T) {
	is := is.New(tst)

	news := []news.Story{
		{
			Url:       "http://example.com",
			Title:     "Crisis on Space Station",
			Published: time.Now(),
			Updated:   time.Now(),
			Category:  news.Good,
		},
		{
			Url:       "http://example.com",
			Title:     "Hell Freezes Over",
			Published: time.Now(),
			Updated:   time.Now(),
			Category:  news.Good,
		},
	}
	text, err := toHTML(news)
	is.NoErr(err)

	fmt.Printf("%s\n", text)

	is.True(strings.Contains(text, `<a href="http://example.com">Hell Freezes Over</a>`))

}

func TestRun(t *testing.T) {
	is := is.New(t)
	sess := awsgo.CreateSession()
	ddb := dynamodb.New(sess)
	ses := sesv2.New(sess)

	reporter := NewReporter(ses, ddb)

	err := reporter.Run()
	is.NoErr(err)
}
