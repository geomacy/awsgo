package main

import (
	"fmt"

	"bitbucket.org/geomacy/awsgo/news/collector"
	"bitbucket.org/geomacy/awsgo/news/watcher"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sqs"
)

func run() error {
	sess, err := session.NewSession()
	if err != nil {
		return fmt.Errorf("failed to establish AWS session: %w", err)
	}

	sqsAPI := sqs.New(sess)
	ddb := dynamodb.New(sess)

	c, err := collector.CreateCollector(sqsAPI, ddb, watcher.QueueName)
	if err != nil {
		return fmt.Errorf("collector creation failed: %w", err)
	}

	err = c.CollectStories()
	if err != nil {
		return fmt.Errorf("failure collecting: %w", err)
	}
	return nil
}

func main() {
	lambda.Start(run)
}
