package main

import (
	"bitbucket.org/geomacy/awsgo/news/watcher"
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sqs"
)

func run() error {

	sess, err := session.NewSession()
	if err != nil {
		return fmt.Errorf("failed to create session: %w", err)
	}

	sqsAPI := sqs.New(sess)
	ddbAPI := dynamodb.New(sess)

	w, err := watcher.CreateWatcher(sqsAPI, ddbAPI, watcher.QueueName)
	if err != nil {
		return fmt.Errorf("failed creating watcher for queue %s: %w", watcher.QueueName, err)
	}

	err = w.Watch()
	if err != nil {
		return fmt.Errorf("failed watching queue %s: %w", watcher.QueueName, err)
	}

	return nil
}

func main() {
	lambda.Start(run)
}
