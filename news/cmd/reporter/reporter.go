package main

import (
	"fmt"

	"bitbucket.org/geomacy/awsgo/news/reporter"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sesv2"
)

func run() error {

	sess, err := session.NewSession()
	if err != nil {
		return fmt.Errorf("failed to create session: %w", err)
	}

	sesAPI := sesv2.New(sess)

	ddbAPI := dynamodb.New(sess)

	reporter := reporter.NewReporter(sesAPI, ddbAPI)

	err = reporter.Run()
	if err != nil {
		return fmt.Errorf("failed to run reporter: %w", err)
	}

	return nil
}

func main() {
	lambda.Start(run)
}
