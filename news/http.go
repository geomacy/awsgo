package news

import (
	"net/http"
	"time"
)

func GetResource(href string) (*http.Response, error) {
	client := http.Client{
		Timeout: 10 * time.Second,
	}
	return client.Get(href)
}
