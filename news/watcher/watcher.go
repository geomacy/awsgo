package watcher

import (
	"fmt"

	"bitbucket.org/geomacy/awsgo/news"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

const (
	QueueName = "stories"
)

type watcher struct {
	ddb database
	out outputQueue
}

func CreateWatcher(sqsAPI sqsiface.SQSAPI, ddbAPI dynamodbiface.DynamoDBAPI, queue string) (watcher, error) {
	q, e := getOutboundQueue(sqsAPI, queue)
	if e != nil {
		return watcher{}, fmt.Errorf("failed to create output queue: %w", e)
	}
	db := createDBClient(ddbAPI)
	return watcher{db, q}, nil
}

func (w watcher) Watch() error {
	feeds, err := w.ddb.GetFeeds()
	if err != nil {
		return fmt.Errorf("failed to start database: %w", err)
	}
	for _, f := range feeds {
		feedResponse, err := news.GetResource(f.Url)
		if err != nil {
			fmt.Printf("could not load feed %s, skipping. Error was: %s", f.Name, err)
			continue
		}
		defer func() { _ = feedResponse.Body.Close() }()

		loader := Loader{w.out}
		if err = loader.Ingest(feedResponse.Body); err != nil {
			fmt.Printf("could not read feed %s, skipping. Error was: %s", f.Name, err)
			continue
		}
	}
	return nil
}

func getOutboundQueue(awsSQS sqsiface.SQSAPI, name string) (outputQueue, error) {
	q := outputQueue{SQSAPI: awsSQS}
	u, e := q.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName: aws.String(name),
	})
	if e != nil {
		return outputQueue{}, fmt.Errorf("error retrieving story queue URL: %w", e)
	}
	q.queueURL = u.QueueUrl
	return q, nil
}

type outputQueue struct {
	sqsiface.SQSAPI
	queueURL *string
}

func (q outputQueue) Consume(s news.Story) error {
	awsStringType := aws.String("String")

	_, err := q.SendMessage(&sqs.SendMessageInput{
		DelaySeconds: aws.Int64(10),
		MessageAttributes: map[string]*sqs.MessageAttributeValue{
			news.UrlKey: {
				DataType:    awsStringType,
				StringValue: aws.String(s.Url),
			},
			news.PublishedKey: {
				DataType:    awsStringType,
				StringValue: aws.String(s.PublishedTime()),
			},
			news.UpdatedKey: {
				DataType:    awsStringType,
				StringValue: aws.String(s.UpdatedTime()),
			},
		},
		MessageBody: aws.String(s.Title),
		QueueUrl:    q.queueURL,
	})
	return err
}
