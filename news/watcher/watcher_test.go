package watcher

import (
	"bitbucket.org/geomacy/awsgo"
	"errors"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/sqs"
	"os"
	"strings"
	"testing"

	"github.com/matryer/is"
)

func TestOutputQueue_Consume(t *testing.T) {
	is := is.New(t)
	rss, err := os.Open("test-rss.xml")
	is.NoErr(err)
	defer rss.Close()

	sqsAPI := sqs.New(awsgo.CreateSession())
	w, err := getOutboundQueue(sqsAPI, QueueName)
	is.NoErr(err)
	testLoader := Loader{w}

	err = testLoader.Ingest(rss)
	is.NoErr(err)
}

func TestWatcherWatch(t *testing.T) {
	is := is.New(t)

	sess := awsgo.CreateSession()
	sqsAPI := sqs.New(sess)
	ddbAPI := dynamodb.New(sess)

	w, err := CreateWatcher(sqsAPI, ddbAPI, QueueName)
	is.NoErr(err)

	is.NoErr(w.Watch())
}

func TestGetOutboundQueueError(t *testing.T) {
	is := is.New(t)

	sess := awsgo.CreateSession()
	sqsAPI := sqs.New(sess)
	_, err := getOutboundQueue(sqsAPI, "bogus")
	is.True(err != nil)

	is.True(strings.HasPrefix(err.Error(), "error retrieving story queue URL"))

	var awe awserr.Error
	if errors.As(err, &awe) {
		is.Equal("AWS.SimpleQueueService.NonExistentQueue", awe.Code())
		is.Equal("The specified queue does not exist for this wsdl version.", awe.Message())
	} else {
		is.Fail() // err should have been convertible to awserr.Error
	}
}
