package watcher

import (
	"bitbucket.org/geomacy/awsgo/news"
	"fmt"
	"io"
	"time"

	"github.com/mmcdole/gofeed"
)

type StoryConsumer interface {
	Consume(news.Story) error
}

type Loader struct {
	Destination StoryConsumer
}

func (l Loader) Ingest(source io.Reader) error {
	fp := gofeed.NewParser()
	feed, err := fp.Parse(source)
	if err != nil {
		return fmt.Errorf("could not parse source: %w", err)
	}

	for i, item := range feed.Items {
		var updatedTime time.Time
		if item.UpdatedParsed != nil {
			updatedTime = *item.UpdatedParsed
		}
		s := news.Story{
			Url:       item.Link,
			Title:     item.Title,
			Published: *item.PublishedParsed,
			Updated:   updatedTime,
			Category:  news.Unknown,
		}
		if err := l.Destination.Consume(s); err != nil {
			return fmt.Errorf("error consuming story %d of %d: %s: %w", i, len(feed.Items), item.Title, err)
		}
	}
	fmt.Printf("%d stories loaded\n", len(feed.Items))
	return nil
}
