package watcher

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

const (
	feedTable = "Feeds"
)

type Feed struct {
	Name, Url string
}

type database struct {
	dynamodbiface.DynamoDBAPI
}

func createDBClient(ddbAPI dynamodbiface.DynamoDBAPI) database {
	return database{ddbAPI}
}

func (ddb database) AddFeed(f Feed) (*dynamodb.PutItemOutput, error) {

	// How cool is this?  Turn an object into a slice of fields for DynamoDB in one line.
	feed, err := dynamodbattribute.MarshalMap(f)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare news feed %s", f)
	}

	newFeed := dynamodb.PutItemInput{
		TableName: aws.String(feedTable),
		Item:      feed,
	}
	output, err := ddb.PutItem(&newFeed)
	if err != nil {
		return nil, fmt.Errorf("failed to add item (%s) to database: %w", f.Name, err)
	}
	return output, nil
}

func (ddb database) GetFeeds() ([]Feed, error) {
	result, err := ddb.Scan(&dynamodb.ScanInput{TableName: aws.String(feedTable)})
	if err != nil {
		return nil, fmt.Errorf("failed to read table %s: %s", feedTable, err)
	}

	results := make([]Feed, len(result.Items))
	for i, it := range result.Items {
		var feed Feed
		err := dynamodbattribute.UnmarshalMap(it, &feed)
		if err != nil {
			fmt.Printf("couldn't read feed details from %+v", it)
			continue
		}
		results[i] = feed
	}

	return results, nil
}

func (ddb database) DeleteFeed(name string) (*dynamodb.DeleteItemOutput, error) {

	feed := map[string]*dynamodb.AttributeValue{
		"Name": {S: &name},
	}

	feedItem := dynamodb.DeleteItemInput{
		TableName: aws.String(feedTable),
		Key:       feed,
	}
	output, err := ddb.DeleteItem(&feedItem)
	if err != nil {
		return nil, fmt.Errorf("failed deleting feed (%s) from database: %w", name, err)
	}
	return output, err
}
