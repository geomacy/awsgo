package watcher

import (
	"fmt"
	"os"
	"sort"
	"testing"

	"bitbucket.org/geomacy/awsgo"
	"bitbucket.org/geomacy/awsgo/news"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/matryer/is"
)

type testConsumer struct{}

func (c testConsumer) Consume(s news.Story) (err error) {
	_, err = fmt.Printf("%+v\n", s)
	return
}

func TestReadFeedRss(t *testing.T) {
	is := is.New(t)
	rss, err := os.Open("test-rss.xml")
	is.NoErr(err)
	defer func() { _ = rss.Close() }()

	testLoader := Loader{testConsumer{}}

	err = testLoader.Ingest(rss)
	is.NoErr(err)
}

func TestStoreFeed(t *testing.T) {
	is := is.New(t)
	sess := awsgo.CreateSession()
	client := createDBClient(dynamodb.New(sess))

	testDatabaseAddFeed(is, client)
	testDatabaseGetFeeds(is, client)
	testDatabaseDeleteFeed(is, client)
}

func testDatabaseAddFeed(is *is.I, client database) {

	_, err := client.AddFeed(Feed{"BBC", "http://feeds.bbci.co.uk/news/rss.xml"})
	is.NoErr(err)
	_, err = client.AddFeed(Feed{"Belfast", "https://news.google.com/topics/CAAqJQgKIh9DQkFTRVFvSUwyMHZNREZzTmpNU0JXVnVMVWRDS0FBUAE?hl=en-GB&amp;gl=GB&amp;ceid=GB:en"})
	is.NoErr(err)
}

func testDatabaseGetFeeds(is *is.I, client database) {

	feeds, err := client.GetFeeds()
	is.NoErr(err)
	is.Equal(2, len(feeds))

	sort.Slice(feeds, func(i, j int) bool {
		return feeds[i].Name < feeds[j].Name
	})

	is.Equal("BBC", feeds[0].Name)
	is.Equal("http://feeds.bbci.co.uk/news/rss.xml", feeds[0].Url)

	is.Equal("Belfast", feeds[1].Name)
	is.Equal("https://news.google.com/topics/CAAqJQgKIh9DQkFTRVFvSUwyMHZNREZzTmpNU0JXVnVMVWRDS0FBUAE?hl=en-GB&amp;gl=GB&amp;ceid=GB:en",
		feeds[1].Url)
}

func testDatabaseDeleteFeed(is *is.I, client database) {

	_, err := client.DeleteFeed("BBC")
	is.NoErr(err)

	_, err = client.DeleteFeed("Belfast")
	is.NoErr(err)

	feeds, err := client.GetFeeds()
	is.NoErr(err)
	is.Equal(0, len(feeds))

}

func TestDeleteFeedIdempotent(t *testing.T) {
	is := is.New(t)
	sess := awsgo.CreateSession()
	client := createDBClient(dynamodb.New(sess))

	_, err := client.DeleteFeed("Bigfoot")
	is.NoErr(err) // deleting a non-existent record is not an error
}
