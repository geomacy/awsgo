module bitbucket.org/geomacy/awsgo

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/aws/aws-lambda-go v1.13.3
	github.com/aws/aws-sdk-go v1.27.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/matryer/is v1.2.0
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/net v0.0.0-20191014212845-da9a3fd4c582 // indirect
)
