package ec2instance

import (
	"bitbucket.org/geomacy/awsgo"
	"bitbucket.org/geomacy/awsgo/names"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/stretchr/testify/assert"
)

const keypairName = "geoff-test2"

func TestCreateAndWait(t *testing.T) {
	s := awsgo.CreateSession()
	ic, h := CreateInstanceSync(s, names.AwsLinux, "t2.micro", keypairName)
	assert.Nil(t, h)

	desc, err := ic.Describe()
	assert.Nil(t, err)
	assert.Equal(t, ec2.InstanceStateNameRunning, *desc.State.Name)

	id := ic.InstanceID

	te := ic.Terminate()
	assert.Nil(t, te)

	testClient := ec2.New(s)
	description, err := testClient.DescribeInstances(&ec2.DescribeInstancesInput{
		InstanceIds: []*string{&id},
	})
	assert.Nil(t, err)
	assert.Equal(t, ec2.InstanceStateNameTerminated, *description.Reservations[0].Instances[0].State.Name)
}

func TestDescribe(t *testing.T) {
	ic, err := CreateInstance(awsgo.CreateSession(), names.AwsLinux, "t2.micro", keypairName)
	assert.Nil(t, err)

	result, err := ic.Describe()
	assert.Nil(t, err)

	fmt.Printf("description: %+v\n", result)
}

func TestCreateNetworkInterface(t *testing.T) {
	ic, err := CreateInstance(awsgo.CreateSession(), names.AwsLinux, "t2.micro", keypairName)
	assert.Nil(t, err)

	iface, err := ic.CreateNetworkInterface("ni-test")
	assert.Nil(t, err)

	fmt.Printf("interface ID: %+v\n", iface)
}

func TestAddNetworkInterface(t *testing.T) {
	ic, err := CreateInstanceSync(awsgo.CreateSession(), names.AwsLinux, "t2.micro", keypairName)
	assert.Nil(t, err)

	iface, err := ic.AddNetworkInterface("ni-test-add-nif", int64(1))
	assert.Nil(t, err)

	fmt.Printf("interface ID: %+v\n", iface)
}
