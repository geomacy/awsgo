package ec2instance

import (
	"fmt"
	"sync"

	"bitbucket.org/geomacy/awsgo/names"
)

func AddInterfaces(ic *EC2InstanceClient) error {

	var errInAdd error
	var wg sync.WaitGroup
	wg.Add(2)

	addIf := func(name string, deviceIndex int64) {
		_, err := ic.AddNetworkInterface(names.Label(ic.InstanceID, name), deviceIndex)
		if err != nil {
			errInAdd = err
		}
		wg.Done()
	}

	go addIf("awsgo_secondary", int64(1))
	go addIf("awsgo_tertiary", int64(2))

	wg.Wait()

	if errInAdd != nil {
		return fmt.Errorf("failed to create node: %w", errInAdd)
	}

	return nil
}
