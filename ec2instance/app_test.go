package ec2instance

import (
	"bitbucket.org/geomacy/awsgo"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"testing"
	"time"

	"bitbucket.org/geomacy/awsgo/names"
	"github.com/matryer/is"
)

const (
	defaultSSHPort = "22"
)

func TestCreateNode(t *testing.T) {

	is := is.New(t)
	s := awsgo.CreateSession()
	ic, err := CreateInstanceSync(s, names.Freebsd11, "t2.small", keypairName)
	is.NoErr(err)

	createErr := AddInterfaces(ic)
	is.NoErr(createErr)
}

func TestSshToNode(t *testing.T) {
	is := is.New(t)
	home := os.Getenv("HOME")
	keyPemFile := path.Join(home, "AWS", "ec2", "geoff-test2.pem")

	privateKey, err := ioutil.ReadFile(keyPemFile)
	is.NoErr(err)

	s := awsgo.CreateSession()
	ic, err := CreateInstanceSync(s, names.AwsLinux, "t2.small", keypairName)
	is.NoErr(err)

	createErr := AddInterfaces(ic)
	is.NoErr(createErr)
	fmt.Printf("Host %s created\n", ic.InstanceID)

	ec2inst, err := ic.Describe()
	is.NoErr(err)
	fmt.Printf("Host %s details: %+v\n", ic.InstanceID, ec2inst)

	fmt.Println("give time for node to get to the point of accepting incoming SSH...")
	time.Sleep(20 * time.Second)
	fmt.Println("...proceeding")

	user := "ec2-user"
	conn, err := ic.ConnectSSH(defaultSSHPort, user, privateKey)
	is.NoErr(err)

	command := "id"
	fmt.Printf("Sending command: %s\n", command)
	response, err := conn.Run(command)
	is.NoErr(err)
	fmt.Printf("response from command: %s\n", response)

	for _, s := range []string{"uid", "gid", "groups", user} {
		is.True(strings.Contains(response, s))
	}

	err = conn.Close()
	is.NoErr(err)

	te := ic.Terminate()
	assert.Nil(t, te)
}

func TestSshToHardcodedNode(t *testing.T) {
	is := is.New(t)
	home := os.Getenv("HOME")
	keyPemFile := path.Join(home, "AWS", "ec2", "geoff-test2.pem")

	privateKey, err := ioutil.ReadFile(keyPemFile)
	is.NoErr(err)

	instance, err := NewClientForInstance(awsgo.CreateSession(), "i-077b3de28135c14ba")
	is.NoErr(err)

	user := "ec2-user"
	conn, err := instance.ConnectSSH(defaultSSHPort, user, privateKey)
	is.NoErr(err)

	response, err := conn.Run("echo hello world")
	is.NoErr(err)
	is.Equal(response, "hello world")
	fmt.Printf("response from command: %s\n", response)

	response, err = conn.Run("id")
	is.NoErr(err)

	fmt.Printf("response from command: %s\n", response)

	for _, s := range []string{"uid", "gid", "groups", user} {
		is.True(strings.Contains(response, s))
	}

	err = conn.Close()
	is.NoErr(err)
}
