package ec2instance

import (
	"fmt"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"

	"bitbucket.org/geomacy/awsgo/names"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	"github.com/gofrs/uuid"
)

// EC2InstanceClient bundles an EC2 client and an instance ID, to manage one EC2 instance
type EC2InstanceClient struct {
	svc        ec2iface.EC2API
	InstanceID string
}

func NewEC2InstanceClient(sess *session.Session) *EC2InstanceClient {
	return &EC2InstanceClient{svc: ec2.New(sess)}
}

func NewClientForInstance(sess *session.Session, instanceID string) (*EC2InstanceClient, error) {

	node := NewEC2InstanceClient(sess)

	node.InstanceID = instanceID

	_, err := node.Describe()
	if err != nil {
		return nil, fmt.Errorf("failed to attach to %s: %s", instanceID, err)
	}

	return node, nil
}

// CreateInstance creates an instance of the image, configured with the given key
func CreateInstance(sess *session.Session, imageID, instanceType, keyName string) (*EC2InstanceClient, error) {

	node := NewEC2InstanceClient(sess)

	runResult, err := node.svc.RunInstances(&ec2.RunInstancesInput{
		// An Amazon Linux AMI ID for t2.micro instances in the us-west-2 region
		ImageId:      aws.String(imageID),
		InstanceType: aws.String(instanceType),
		KeyName:      aws.String(keyName),
		MinCount:     aws.Int64(1),
		MaxCount:     aws.Int64(1),
	})

	fmt.Printf("RunInstances result: %+v\n", runResult)

	if err != nil {
		return nil, fmt.Errorf("could not create instance: %s", err)
	}

	instance := runResult.Instances[0]
	uid, err := uuid.NewV4()
	if err != nil {
		return nil, fmt.Errorf("internal error creating instance %s", err)
	}
	_, errtag := node.svc.CreateTags(&ec2.CreateTagsInput{
		Resources: []*string{instance.InstanceId},
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(names.Label("awsgo", uid.String())),
			}, {
				Key:   aws.String("use"),
				Value: aws.String("gotest"),
			},
		},
	})
	if errtag != nil {
		return nil, fmt.Errorf("could not create tags for instance %s: %s", *instance.InstanceId, errtag)
	}

	node.InstanceID = *instance.InstanceId
	return node, nil
}

// CreateInstanceSync creates the image and waits for it to be running, returning the instance ID
func CreateInstanceSync(sess *session.Session, imageID, instanceType, keyName string) (*EC2InstanceClient, error) {

	inst, err := CreateInstance(sess, imageID, instanceType, keyName)
	if err != nil {
		return nil, err
	}

	err = inst.awaitInstanceRunning(inst.InstanceID)
	if err != nil {
		return nil, fmt.Errorf("%s did not start up: %s", inst.InstanceID, err)
	}

	return inst, nil
}

func isInstance(inst *ec2.Instance, instanceState string) bool {
	n := inst.State.Name
	return n != nil && *n == instanceState
}

func (ei *EC2InstanceClient) Terminate() error {
	thisInstance := ei.InstanceID
	_, err := ei.svc.TerminateInstances(&ec2.TerminateInstancesInput{
		InstanceIds: []*string{&thisInstance},
	})
	if err != nil {
		return err
	}
	ei.InstanceID = ""
	return ei.svc.WaitUntilInstanceTerminated(&ec2.DescribeInstancesInput{
		InstanceIds: []*string{&thisInstance},
	})
}

func (ei *EC2InstanceClient) awaitInstanceRunning(instID string) error {
	tick := time.NewTicker(3 * time.Second)
	timeout := time.After(5 * time.Minute)
	readyC := make(chan struct{})
	errC := make(chan error)

	go func() {
		for {
			select {
			case <-timeout:
				errC <- fmt.Errorf("timed out waiting for %s", instID)
				return
			case <-tick.C:
				current, err := ei.Describe()
				if err != nil {
					errC <- err
					return
				}
				state := current.State
				fmt.Printf("%s is %s\n", instID, *state.Name)
				if isInstance(current, ec2.InstanceStateNameRunning) {
					close(readyC)
					return
				} else if !isInstance(current, ec2.InstanceStateNamePending) {
					errC <- fmt.Errorf("%s became %s while waiting", instID, *state.Name)
					return
				}
			}
		}
	}()

	select {
	case err := <-errC:
		return fmt.Errorf("failed waiting for instance to run: %w", err)
	case <-readyC:
		return nil
	}
}

func (ei *EC2InstanceClient) CreateNetworkInterface(description string) (string, error) {

	instance, err := ei.Describe()
	if err != nil {
		return "", fmt.Errorf("failed to get instance %s information: %s", ei.InstanceID, err)
	}

	input := ec2.CreateNetworkInterfaceInput{
		Description: aws.String(description),
		DryRun:      aws.Bool(false),
		Groups:      []*string{aws.String(*instance.SecurityGroups[0].GroupId)},
		SubnetId:    aws.String(*instance.SubnetId),
	}

	output, err := ei.svc.CreateNetworkInterface(&input)
	if err != nil {
		return "", fmt.Errorf("error creating network interface %s: %s", description, err)
	}

	fmt.Printf("created nework interface %+v\n", output)
	return *output.NetworkInterface.NetworkInterfaceId, nil
}

func (ei *EC2InstanceClient) AwaitNetworkInterface(interfaceID, from, to string) error {

	tick := time.NewTicker(1 * time.Second)
	timeout := time.After(5 * time.Minute)
	readyC := make(chan struct{})
	errC := make(chan error)

	input := ec2.DescribeNetworkInterfacesInput{
		NetworkInterfaceIds: []*string{&interfaceID},
	}

	go func() {
		for {
			select {
			case <-timeout:
				errC <- fmt.Errorf("timed out waiting for %s", interfaceID)
				return
			case <-tick.C:
				output, err := ei.svc.DescribeNetworkInterfaces(&input)
				if err != nil {
					errC <- fmt.Errorf("error getting network interface %s information: %s", interfaceID, err)
					return
				}
				niStatus := *output.NetworkInterfaces[0].Status
				fmt.Printf("network interface %s is %s\n", interfaceID, niStatus)
				if niStatus == to {
					close(readyC)
					return
				} else if niStatus != from {
					errC <- fmt.Errorf("%s became %s while waiting", interfaceID, niStatus)
					return
				}
			}
		}
	}()

	select {
	case err := <-errC:
		return fmt.Errorf("failed waiting for interface %s to attach: %w", interfaceID, err)
	case <-readyC:
		return nil
	}
}

func (ei *EC2InstanceClient) AddNetworkInterface(description string, deviceIndex int64) (string, error) {

	instance, err := ei.Describe()
	if err != nil {
		return "", fmt.Errorf("error getting instance %s information: %s", ei.InstanceID, err)
	}

	iface, err := ei.CreateNetworkInterface(description)
	if err != nil {
		return "", fmt.Errorf("error creating interface: %s", err)
	}

	// why no NetworkInterfaceStatusPending? Using NetworkInterfacePermissionStateCodePending instead
	err = ei.AwaitNetworkInterface(iface, ec2.NetworkInterfacePermissionStateCodePending, ec2.NetworkInterfaceStatusAvailable)
	if err != nil {
		return "", fmt.Errorf("error waiting for %s: %s", iface, err)
	}

	instID := *instance.InstanceId
	_, err = ei.AttachNetworkInterface(instID, iface, deviceIndex)
	if err != nil {
		return "", fmt.Errorf("error attaching interface %d to %s: %s", deviceIndex, instID, err)
	}

	err = ei.AwaitNetworkInterface(iface, ec2.NetworkInterfaceStatusAttaching, ec2.NetworkInterfaceStatusInUse)
	if err != nil {
		return "", fmt.Errorf("error waiting for %s: %s", iface, err)
	}

	return iface, nil
}

func (ei *EC2InstanceClient) AttachNetworkInterface(instanceID, interfaceID string, deviceIndex int64) (string, error) {

	spec := ec2.AttachNetworkInterfaceInput{
		DeviceIndex:        aws.Int64(deviceIndex),
		DryRun:             aws.Bool(false),
		InstanceId:         aws.String(instanceID),
		NetworkInterfaceId: aws.String(interfaceID),
	}

	output, err := ei.svc.AttachNetworkInterface(&spec)
	if err != nil {
		return "", fmt.Errorf("error attaching %s to %s: %s", interfaceID, instanceID, err)
	}
	return *output.AttachmentId, nil
}

func (ei *EC2InstanceClient) Describe() (*ec2.Instance, error) {
	input := &ec2.DescribeInstancesInput{
		InstanceIds: []*string{
			aws.String(ei.InstanceID),
		},
	}

	result, err := ei.svc.DescribeInstances(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				return nil, fmt.Errorf("error describing %s: %s", ei.InstanceID, aerr.Error())
			}
		} else {
			return nil, fmt.Errorf("error describing %s: %s", ei.InstanceID, err.Error())
		}
	}
	return result.Reservations[0].Instances[0], nil
}

func (ei *EC2InstanceClient) ConnectSSH(port, userName string, privateKey []byte) (*SshConnection, error) {

	key, err := ssh.ParsePrivateKey(privateKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key: %s", err)
	}

	instance, err := ei.Describe()
	if err != nil {
		return nil, fmt.Errorf("failed to get instance %s information: %s", ei.InstanceID, err)
	}

	sshConfig := &ssh.ClientConfig{
		User: userName,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(key),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	address := names.Join(":", *instance.PublicDnsName, port)
	fmt.Printf("dialling SSH connection to %s\n", address)
	connection, err := ssh.Dial("tcp", address, sshConfig)
	if err != nil {
		return nil, fmt.Errorf("failed to dial: %s", err)
	}

	return &SshConnection{
		clientConn: connection,
	}, nil
}

type SshConnection struct {
	clientConn *ssh.Client
}

func (sc *SshConnection) Run(cmd string) (string, error) {

	sess, err := sc.clientConn.NewSession()
	if err != nil {
		return "", fmt.Errorf("failed to create session: %s", err)
	}
	defer func() { _ = sess.Close() }()

	bytes, err := sess.CombinedOutput(cmd)
	text := strings.TrimSpace(string(bytes))
	if err != nil {
		return "", fmt.Errorf("command failed: %s", text)
	}

	return text, nil
}

func (sc *SshConnection) Close() error {
	if err := sc.clientConn.Close(); err != nil {
		return fmt.Errorf("error closing connection: %w", err)
	}

	return nil
}
