#!/bin/bash

name="${1}"

GOOS=linux go build -o /tmp/"${name}" bitbucket.org/geomacy/awsgo/news/cmd/"${name}"
zip -j /tmp/"${name}".zip /tmp/"${name}"
aws lambda create-function \
--function-name "${name}" \
--runtime go1.x \
--role arn:aws:iam::${AWS_ACCOUNT}:role/lambda-awsgo \
--handler "${name}" \
--zip-file fileb:///tmp/"${name}".zip