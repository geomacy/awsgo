feeds:
	sh create_feeds_table.sh

stories:
	sh create_stories_table.sh

stories_published:
	sh add_secondary_index.sh Stories published

stories_updated:
	sh add_secondary_index.sh Stories updated

stories_category:
	sh add_secondary_index.sh Stories category

test_feeds:
	sh feed_init.sh

create_watcher:
	sh create_lambda.sh watcher

update_watcher:
	sh update_lambda.sh watcher

create_collector:
	sh create_lambda.sh collector

update_collector:
	sh update_lambda.sh collector

create_reporter:
	sh create_lambda.sh reporter

update_reporter:
	sh update_lambda.sh reporter

collector_image:
	docker build -t geomacy/awsgo/collector:1.0 -f news/cmd/collector/Dockerfile .
