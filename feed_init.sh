aws dynamodb put-item --table-name Feeds --item '
{
    "Name": {"S": "BBCNI"},
    "Url" : {"S": "http://feeds.bbci.co.uk/news/northern_ireland/rss.xml"}
}'

aws dynamodb put-item --table-name Feeds --item '
{
    "Name": {"S": "World"},
    "Url" : {"S": "http://yahoo.com/news/world/rss"}
}'
