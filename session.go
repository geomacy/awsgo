package awsgo

import "github.com/aws/aws-sdk-go/aws/session"

func CreateSession() *session.Session {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	return sess
}
