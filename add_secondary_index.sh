function add_secondary_index() {
  local tname="${1}"
  local iname="${2}"
  local index_json=$(cat secondary_index.json | sed "s/TOKEN/${iname}/g")
  echo
  aws dynamodb update-table \
    --table-name Stories \
    --attribute-definitions AttributeName="${iname}",AttributeType=S \
    --global-secondary-index-updates "${index_json}"
}

add_secondary_index "${@}"